package Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: MatriceCompetence
 *
 */
@Entity

public class MatriceCompetence implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne(mappedBy = "matrice_competence")
	private User employee;
	
	@OneToMany
	private List<Competence> les_competences ;
	
	public MatriceCompetence() {
		super();
	}
   
}

package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: feedback
 *
 */
@Entity

public class Feedback implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String remarque;
	
	@ManyToOne
	private Evaluation evaluation;
	
	private static final long serialVersionUID = 1L;

	public Feedback() {
		super();
	}
   
}

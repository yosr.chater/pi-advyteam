package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Formation
 *
 */
@Entity

public class Formation implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String title;
	private String description;
	
	private Date date_debut;
	private Date date_fin;
	private int nb_participants;
	
	@ManyToOne
	private User formateur;
	
	@ManyToMany
	private List<User> participants;
	
	
	public Formation() {
		super();
	}
   
}

package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ExpenseReport
 *
 */
@Entity

public class ExpenseReport implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	private String type;
	private String location;
	private String description;
	private Double amount =0.0;
	
	@OneToOne
	private Mission mission;
	
	@OneToMany
	private  List<Bill> bill;
	
	
	public List<Bill> getBill() {
		return bill;
	}

	public void setBill(List<Bill> bill) {
		this.bill = bill;
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	private static final long serialVersionUID = 1L;

	public ExpenseReport() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ExpenseReport(int id, Date date, String type, String location, String description, Double amount,
			Mission mission) {
		super();
		this.id = id;
		this.date = date;
		this.type = type;
		this.location = location;
		this.description = description;
		this.amount = amount;
		this.mission = mission;
	}
	
	
   
}

package Entities;

public class ConnectedUser {
	
	private static int idConnected;
	protected String cin;
	protected String Firstname;
	protected String LastName;
	protected String Phone;
	protected String mail;
	protected String login;
	protected String password;
	protected Double salary =0.0;	
	private String speciality;
	private String grade;
	
	// getters and setters 
	
	public static int getIdConnected() {
		return idConnected;
	}
	public String getCin() {
		return cin;
	}
	public String getFirstname() {
		return Firstname;
	}
	public String getLastName() {
		return LastName;
	}
	public String getPhone() {
		return Phone;
	}
	public String getMail() {
		return mail;
	}
	public String getLogin() {
		return login;
	}
	public String getPassword() {
		return password;
	}
	public Double getSalary() {
		return salary;
	}
	public String getSpeciality() {
		return speciality;
	}
	public String getGrade() {
		return grade;
	}
	public static void setIdConnected(int idConnected) {
		ConnectedUser.idConnected = idConnected;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public void setFirstname(String firstname) {
		Firstname = firstname;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}

	



}

package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Competence
 *
 */
@Entity

public class Competence implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
    private Famille famille;
	
	private String description;
	private int niveau;
	
	private String title;
	public Competence(String title, Famille famille, String description, int niveau) {
		super();
		this.title = title;
		this.famille = famille;
		this.description = description;
		this.niveau = niveau;
	}

	

	public int getId() {
		return id;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public Famille getFamille() {
		return famille;
	}

	public void setFamille(Famille famille) {
		this.famille = famille;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Competence() {
		super();
	}
   
}

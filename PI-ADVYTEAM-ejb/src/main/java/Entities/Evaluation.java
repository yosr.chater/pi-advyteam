package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.omg.CORBA.PRIVATE_MEMBER;

/**
 * Entity implementation class for Entity: Evaluation
 *
 */
@Entity

public class Evaluation implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private Date date_eval;
	
	private int score;
	private int obectif_score;
	private TypeEval type;
	
	
	@OneToMany(mappedBy = "evaluation")
	private List<Feedback> feedbacks;
	//changer manytomany
	@OneToMany(mappedBy = "evaluation")
	private List<User> participants;

	
	
	public Evaluation() {
		super();
	}


	public Evaluation(String title, Date date_eval, int score, int obectif_score, TypeEval type) {
		super();
		this.title = title;
		this.date_eval = date_eval;
		this.score = score;
		this.obectif_score = obectif_score;
		this.type = type;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getDate_eval() {
		return date_eval;
	}


	public void setDate_eval(Date date_eval) {
		this.date_eval = date_eval;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public int getObectif_score() {
		return obectif_score;
	}


	public void setObectif_score(int obectif_score) {
		this.obectif_score = obectif_score;
	}


	public TypeEval getType() {
		return type;
	}


	public void setType(TypeEval type) {
		this.type = type;
	}


	public List<User> getParticipants() {
		return participants;
	}


	public void setParticipants(List<User> participants) {
		this.participants = participants;
	}
   
	
}

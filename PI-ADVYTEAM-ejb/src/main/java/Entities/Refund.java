package Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
@Entity
public class Refund {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int id_bill;
	private double amount =0.0;
	private String type;
	
	@OneToOne
	private ExpenseReport expenseReport;
	
	@ManyToOne
	private User financialManager;
	
	
	
	public User getFinancialManager() {
		return financialManager;
	}
	public void setFinancialManager(User financialManager) {
		this.financialManager = financialManager;
	}
	public ExpenseReport getExpenseReport() {
		return expenseReport;
	}
	public void setExpenseReport(ExpenseReport expenseReport) {
		this.expenseReport = expenseReport;
	}
	public Refund() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Refund(int id, int id_bill, double amount, String type) {
		super();
		this.id = id;
		this.id_bill = id_bill;
		this.amount = amount;
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_bill() {
		return id_bill;
	}
	public void setId_bill(int id_bill) {
		this.id_bill = id_bill;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}

package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: FicheMetier
 *
 */
@Entity

public class FicheMetier implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String poste;
	
	@OneToOne
	private MatriceCompetence matrice_competnece;
	
	
	private static final long serialVersionUID = 1L;

	public FicheMetier() {
		super();
	}
   
}

package Services;

import javax.ejb.Remote;

import Entities.Settings;

@Remote
public interface SettingsServicesRRemote {
	public Settings addSettings(Settings settings);
	//public List<Settings> getAllSettings();
}

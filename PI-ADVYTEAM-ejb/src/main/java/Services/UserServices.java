package Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entities.Role;
import Entities.User;

/**
 * Session Bean implementation class UserServices
 */
@Stateless
@LocalBean
public class UserServices implements UserServicesRemote {
	
    @PersistenceContext(unitName = "pi_datasouce")
	EntityManager em;

    /**
     * Default constructor. 
     */
    public UserServices() {
        // TODO Auto-generated constructor stub
    }

    


	@Override
	public User login(String login, String passwd) {
	   	User u =new User();
    	u= em.createQuery("select c from User c where c.login=:l and c.password=:p",User.class)
    			.setParameter("l", login).setParameter("p", passwd).getSingleResult();
    	
    	return u;
	}
	
	
	@Override
	public List<User> getallUser() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public List<User> getAllEmploye(){
		List<User> list = new ArrayList<User>();
		list = em.createQuery("select u from User u where u.role=:role ").setParameter("role", Role.Employee).getResultList();
		return list ;
	}

}

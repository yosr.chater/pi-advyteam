package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.User;

@Remote
public interface UserServicesRemote {
	
	public List<User> getallUser();

	public User login(String login, String passwd);

}

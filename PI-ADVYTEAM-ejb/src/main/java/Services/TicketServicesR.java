package Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Entities.Status;
import Entities.Ticket;

/**
 * Session Bean implementation class TicketServicesR
 */
@Stateless
@LocalBean
public class TicketServicesR implements TicketServicesRRemote {

    /**
     * Default constructor. 
     */
    public TicketServicesR() {
        // TODO Auto-generated constructor stub
    }
       
	@PersistenceContext(unitName = "pi_datasouce")
    EntityManager em;


	@Override
	public void addTicket(Ticket ticket) {
		em.persist(ticket);
		
	}

	@Override
	public List<Ticket> getAllTickets() {
		List <Ticket>  les_Tickets= new ArrayList<>()  ; //
		Query query = em.createQuery("select t from Ticket t"); 
		try {
			les_Tickets = query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return les_Tickets;
	}

	@Override
	public List<Ticket> getTicketsByStatus(Status s) {
		List <Ticket>  les_Tickets= new ArrayList<>()  ; //
		Query query = em.createQuery("select t from Ticket t where t.status=:s"); 
		query.setParameter("s", s);
		try {
			les_Tickets = query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return les_Tickets;
	}

	@Override
	public List<Ticket> getTicketByAssignement(boolean a) {
		List <Ticket>  les_Tickets= new ArrayList<>()  ; //
		Query query = em.createQuery("select t from Ticket t where t.assignement=:a"); 
		query.setParameter("a", a);
		try {
			les_Tickets = query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return les_Tickets;
	}

	@Override
	public void deleteTicketByID(int id) {

		Ticket t;
		t=em.find(Ticket.class, id);
		em.remove(t);
	}

	@Override
	public void assignTicketToEmployee(int idEmploye) {
		// TODO Auto-generated method stub
		
	}
    

}

package Services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entities.Settings;

/**
 * Session Bean implementation class SettingsServicesR
 */
@Stateless
@LocalBean
public class SettingsServicesR implements SettingsServicesRRemote {
	
	@PersistenceContext(unitName = "pi_datasouce")
    EntityManager em;
    /**
     * Default constructor. 
     */
    public SettingsServicesR() {
        // TODO Auto-generated constructor stub
    }
    

 
	@Override
	public Settings addSettings(Settings settings) {
		return em.merge(settings);
		
	}
	
	
	public Settings getSettings() {
		Settings settings = null;
		
		settings = 	(Settings) em.createQuery("select s from Settings s ").setMaxResults(1).getSingleResult();
		return settings ;
	}

}

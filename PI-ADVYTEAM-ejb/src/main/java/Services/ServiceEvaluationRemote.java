package Services;

import javax.ejb.Remote;

import Entities.Evaluation;

@Remote
public interface ServiceEvaluationRemote {
	public int ajouterEval(Evaluation e);
	public void deleteEval(int id);
	public void updateEval(Evaluation e);
	public Evaluation find (int id);
	public Evaluation findByIdM (int id);

}

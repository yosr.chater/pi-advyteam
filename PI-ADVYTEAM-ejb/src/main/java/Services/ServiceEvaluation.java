package Services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entities.Evaluation;

/**
 * Session Bean implementation class ServiceEvaluation
 */
@Stateless
@LocalBean
public class ServiceEvaluation implements ServiceEvaluationRemote {

    /**
     * Default constructor. 
     */
	@PersistenceContext(unitName ="pi_datasouce")
	   EntityManager em;
    public ServiceEvaluation() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public int ajouterEval(Evaluation e) {
		em.persist(e);
		return e.getId();
	}

	@Override
	public void deleteEval(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEval(Evaluation e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Evaluation find(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Evaluation findByIdM(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}

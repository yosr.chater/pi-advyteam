package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.Competence;

@Remote
public interface ServiceCompetenceRemote {
	public int ajouterComp(Competence e);
	public void deleteComp(int id);
	public void updateComp(Competence c);
	public List<Competence> getAllCompetence() ;
		
	
	
	public Competence findByIdComp (int id);

}

package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entities.Competence;
import Entities.Evaluation;

/**
 * Session Bean implementation class ServiceEvaluation
 */
@Stateless
@LocalBean
public class ServiceCompetence implements ServiceCompetenceRemote {

    /**
     * Default constructor. 
     */
	@PersistenceContext(unitName ="pi_datasouce")
	   EntityManager em;
    public ServiceCompetence() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public int ajouterComp(Competence e) {
		em.persist(e);
		return e.getId();
	}
	@Override
	public void deleteComp(int id) {
		em.remove(em.find(Competence.class, id));
		}
	@Override
	public void updateComp(Competence c) {
        em.merge(c);
	}
	@Override
	public List<Competence> getAllCompetence() {
	return(em.createQuery("select c from Competence c ",Competence.class).getResultList());
		
	
	}
	@Override
	public Competence findByIdComp(int id) {
		
	return null ;
	}
	
	
}
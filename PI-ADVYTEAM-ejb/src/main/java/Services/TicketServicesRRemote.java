package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.Status;
import Entities.Ticket;

@Remote
public interface TicketServicesRRemote {

	
	public void addTicket(Ticket ticket);
	public List<Ticket> getAllTickets();
	public List<Ticket> getTicketsByStatus(Status s);
	public List<Ticket> getTicketByAssignement(boolean a);
	public void deleteTicketByID(int id);
	public void assignTicketToEmployee(int idEmploye);
}

package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.Timesheet;
import Entities.User;

@Remote
public interface TimesheetServicesRRemote {
	
	//public List<Timesheet> getShiftsByOwner(User owner);
	public List<Timesheet> getShiftsByOwner(User owner);

}

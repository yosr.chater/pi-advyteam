package ManagedBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;



import Entities.Famille;

@ManagedBean
@ApplicationScoped
public class Fam {
	public Famille[] getFamille(){
		
		return Famille.values();
	}
}

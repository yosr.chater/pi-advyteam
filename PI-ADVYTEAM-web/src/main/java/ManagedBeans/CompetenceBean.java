package ManagedBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import Entities.Competence;

import Entities.Evaluation;
import Entities.Famille;
import Services.ServiceCompetence;
import Services.ServiceEvaluation;

@ManagedBean
public class CompetenceBean {

	private String title;
	
	private Famille famille;
	
	private String description;
	private int niveau;
	@EJB
	ServiceCompetence service;
	

public String addComp() {
		
		service.ajouterComp(new Competence(title, famille, description, niveau));
		
	     return null;
	}
public List<Competence> getCompetences(){
	
	return(service.getAllCompetence());
}
public void supprimer(Integer competenceId){
	service.deleteComp(competenceId);
	}	
public void modifier() {
	

}
public void mettreAjourCompetence() {
	
	service.updateComp(new Competence(title, famille, description, niveau));
	
}




public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public Famille getFamille() {
	return famille;
}

public void setFamille(Famille famille) {
	this.famille = famille;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public int getNiveau() {
	return niveau;
}

public void setNiveau(int niveau) {
	this.niveau = niveau;
}

public ServiceCompetence getService() {
	return service;
}

public void setService(ServiceCompetence service) {
	this.service = service;
}




}

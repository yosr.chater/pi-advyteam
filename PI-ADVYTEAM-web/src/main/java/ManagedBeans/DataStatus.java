package ManagedBeans;

import javax.faces.bean.ManagedBean;

import Entities.Status;
@ManagedBean
public class DataStatus {

	public Status[] getStatus()
	{
		return Status.values();
	}
}

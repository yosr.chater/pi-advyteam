package ManagedBeans;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import Entities.Evaluation;
import Entities.TypeEval;
import Services.ServiceEvaluation;

@ManagedBean
public class EvaluationBean implements Serializable {
	private int score;
	private int obectif_score;
	private String title;
	private Date date_eval;
	private TypeEval type;
	
	@EJB
	ServiceEvaluation service;
	
	public String addEval() {
		
		service.ajouterEval(new Evaluation(title,date_eval,obectif_score,score,type));
		
	     return null;
	}
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getObectif_score() {
		return obectif_score;
	}
	public void setObectif_score(int obectif_score) {
		this.obectif_score = obectif_score;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getDate_eval() {
		return date_eval;
	}
	public void setDate_eval(Date date_eval) {
		this.date_eval = date_eval;
	}
	public TypeEval getType() {
		return type;
	}
	public void setType(TypeEval type) {
		this.type = type;
	}
	
	

}

package ManagedBeans;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import Entities.TypeEval;
@ManagedBean
@ApplicationScoped
public class Data {
	public TypeEval[] getTypes() {
		
		return TypeEval.values();
		
		
	}

}
